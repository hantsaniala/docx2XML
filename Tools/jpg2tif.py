import os, glob
from PIL import Image

file_list = glob.glob(os.path.join( "*.jpg"))
os.makedirs("tif")
#os.chdir("ok")
for file_name in file_list:
    print(file_name)
    name = file_name.split(".")[0]
    img = Image.open(name+'.jpg')
    img.save(os.path.join('tif', name+'.tif'), 'TIFF', dpi=(150, 150))
