#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import sys
import optparse
import re
import shutil
import glob
import zipfile
from lxml import etree
import lxml.html as LH
from io import StringIO
from posixpath import join
from subprocess import Popen, PIPE


# TODO :
# - Covert all code to OOP struct
# - Using design pattern
# - Adding GUI (PyQt5, Django, Flask or something else...)
# - Create Windows executable (PyWin32, cx_freeze, isetup)

def saxonWrapper(*args):
    process = Popen(list(args), stdout=PIPE, stderr=PIPE)
    ret = process.communicate()
    return ret


# Get file name
def get_file():
    """
    Get the file name from the terminal.
    """
    parser = optparse.OptionParser(
        "Utilisation : python XML2ePub.py -f <file_name>.xml")
    parser.add_option('-f', dest='fname', type='string',
                      help='spécifié un fichier')
    (options, args) = parser.parse_args()

    if (options.fname == None):
        print("[x] Veuillez spécifier un nom de fichier !")
        print(parser.usage)
        sys.exit()
    else:
        fname = options.fname
    return fname


def copy_graphics(file):
    """
    Get if the gallery folder exist and copy all pictures to the tmp folder
    """
    print("[i] Copie des images...")
    path, filename = os.path.split(file)  # /path//file.xml
    file_name = filename.split(".")[0]  # file
    file_ext = filename.split(".")[1]  # .xml

    if (os.path.isdir(os.path.join(path, file_name))):  # If graphics folder exist
        jpg_list = glob.glob(os.path.join(path, file_name, "*.jpg"))
        png_list = glob.glob(os.path.join(path, file_name, "*.png"))

        # Copy all graphics to temporary folder
        for jpg in jpg_list:
            shutil.copy(jpg, os.path.join(path, "tmp", "OEBPS", "Images"))
        for png in png_list:
            shutil.copy(png, os.path.join(path, "tmp", "OEBPS", "Images"))


def copy_css(file):
    """
    Copy all CSS files in tmp folder
    """
    print("[i] Copie des feuilles de style...")
    path, filename = os.path.split(file)  # /path//file.xml
    file_name = filename.split(".")[0]  # file
    file_ext = filename.split(".")[1]  # .xml

    css_list = glob.glob(os.path.join(
        "D:\\", "git", "docx2XML", "CSS", "*.css"))

    # Copy all stylesheets to temporary folder
    for css_file in css_list:
        shutil.copy(os.path.join(path, file_name, css_file),
                    os.path.join(path, "tmp", "OEBPS", "Styles"))


# TODO : If file already exist
def zip_folder(file):
    """
    Create a zip file, add all tmp folder item to it and rename to epub after.
    """
    print("[i] Création du fichier ePub...")
    path, filename = os.path.split(file)

    try:  # Adding the content of the tmp folder to the zip file
        shutil.make_archive(os.path.join(path, filename.split(".")[
                            0]), "zip", os.path.join(path, "tmp"))
    except:
        print("/!\ Archive Zip déjà existant !")

    try:  # Rename the file .zip to .epub
        os.rename(os.path.join(path, filename.split(".")[
                  0] + ".zip"), os.path.join(path, filename.split(".")[0] + ".epub"))
    except:
        print("/!\ Archive Zip non trouvé ou fichier epub déjà existant !")

        # TEMP : Remove existing file
        os.remove(os.path.join(path, filename.split(".")[0] + ".epub"))
        print("/!\ Ecrasement du fichier !")
        os.rename(os.path.join(path, filename.split(".")[
                  0] + ".zip"), os.path.join(path, filename.split(".")[0] + ".epub"))

    try:  # Remove temporary files after conversion
        print("[i] Suppréssion des fichiers temporaires")
        shutil.rmtree(os.path.join(path, "tmp"))
        shutil.rmtree("D:\\Khadafi")
        os.remove(os.path.join(path, "001.xml"))
        os.remove(os.path.join(path, "002.xml"))
        os.remove(os.path.join(path, "003.xml"))
    except:
        print("/!\ Fichiers temporaire non trouvé !")


def tmp_folder(file):
    """
    Create a tmp folder if it doesn't exist.
    Create the ePub squeletton.
    META-INF, OEBPS, Images, Styles, Text
    """

    path, filename = os.path.split(file)
    try:
        if os.path.isdir(os.path.join(path, "tmp")):
            print("/!\ Répertoire déjà existant !")
            print("/!\ Netoyage !")
            # Avoid merging multiple unnecessary files
            shutil.rmtree(os.path.join(path, "tmp"))

        print("[i] Création des fichiers temporaires...")
        os.mkdir(os.path.join(path, "tmp"))
        os.mkdir(os.path.join(path, "tmp", "META-INF"))
        os.mkdir(os.path.join(path, "tmp", "OEBPS"))
        os.mkdir(os.path.join(path, "tmp", "OEBPS", "Images"))
        os.mkdir(os.path.join(path, "tmp", "OEBPS", "Styles"))
        os.mkdir(os.path.join(path, "tmp", "OEBPS", "Text"))

        # TODO : using UUID as temporary folder name
        os.mkdir("D:\\Khadafi")
    except:
        print("/!\ Répertoire déjà existant !")

    tmp_path = os.path.join(path, "tmp")

    return tmp_path


def get_metadata(file_name):
    """
    Get metadata from the header of the XML file :
    Title, Author, Funder, ISBN
    """
    
    # TODO :
    # Generate toc.ncx and content.opf from the XML with XSLT
    import uuid
    u = uuid.uuid1()
    file = etree.parse(file_name)
    TEI = file.getroot()
    NSMAP = {"tei": "http://www.tei-c.org/ns/1.0"}

    bk_title = bk_author = bk_funder = bk_isbn = bk_publisher = bk_copyright = ""

    book_title = file.xpath(
        '//tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type="main"]/text()', namespaces=NSMAP)
    book_author = file.xpath(
        '//tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author[@role="auteur"]/text()', namespaces=NSMAP)
    book_funder = file.xpath(
        '//tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:funder/text()', namespaces=NSMAP)
    book_isbn = file.xpath(
        '//tei:teiHeader/tei:fileDesc/tei:publicationStmt/tei:idno[@type="EAN_ePUB"]/text()', namespaces=NSMAP)
    book_publisher = file.xpath(
        '//tei:teiHeader/tei:fileDesc/tei:publicationStmt/tei:publisher/text()', namespaces=NSMAP)
    # BUG : Incomplete received values
    book_copyright = file.xpath(
        '//tei:teiHeader/tei:fileDesc/tei:publicationStmt/tei:availability/tei:licence/text()', namespaces=NSMAP)

    # Les autres métadonnées sont a voir après
    if (book_title != []):
        bk_title = book_title[0]
    if (book_author != []):
        bk_author = book_author[0]
    if (book_funder != []):
        bk_funder = book_funder[0]
    if (book_publisher != []):
        bk_publisher = book_publisher[0]
    if (book_copyright != []):
        bk_copyright = book_copyright[0]

    book_metadata = {}
    book_metadata = {
        "title": bk_title,
        "author": bk_author,
        "funder": bk_funder,
        "isbn": bk_isbn,
        "uuid": u.urn,
        "publisher": bk_publisher,
        "copyright": bk_copyright
    }
    return book_metadata


def generate_container(file, bk_metadata):
    """
    Generate the container.opf file
    """
    path, filename = os.path.split(file)

    idpf = "http://www.idpf.org/2007/opf"
    opf = "{%s}" % idpf
    NSMAP = {
        None: idpf
    }
    NSMAP2 = {
        "dc": "http://purl.org/dc/elements/1.1/",
        "opf": "http://www.idpf.org/2007/opf"
    }

    # Tree
    package = etree.Element(
        opf + "package", attrib={"unique-identifier": "BookId", "version": "2.0"}, nsmap=NSMAP)
    tree = etree.ElementTree(package)
    metadata = etree.SubElement(package, "metadata", nsmap=NSMAP2)
    manifest = etree.SubElement(package, "manifest")
    spine = etree.SubElement(package, "spine", attrib={"toc": "ncx"})
    guide = etree.SubElement(package, "guide")

    # Metadata
    urn = etree.SubElement(metadata, "{http://purl.org/dc/elements/1.1/}identifier", attrib={
                           "id": "BookId", "{http://www.idpf.org/2007/opf}scheme": "UUID"}, nsmap=NSMAP2)
    urn.text = bk_metadata["uuid"]
    title = etree.SubElement(
        metadata, "{http://purl.org/dc/elements/1.1/}title", nsmap=NSMAP2)
    title.text = bk_metadata["title"]
    author = etree.SubElement(metadata, "{http://purl.org/dc/elements/1.1/}creator", attrib={
                              "{http://www.idpf.org/2007/opf}role": "aut"}, nsmap=NSMAP2)
    author.text = bk_metadata["author"]
    identifier = etree.SubElement(metadata, "{http://purl.org/dc/elements/1.1/}identifier", attrib={
                                  "{http://www.idpf.org/2007/opf}scheme": "ISBN"}, nsmap=NSMAP2)
    identifier.text = bk_metadata["isbn"]
    language = etree.SubElement(
        metadata, "{http://purl.org/dc/elements/1.1/}language", nsmap=NSMAP2)
    language.text = "fr-FR"
    generator = etree.SubElement(metadata, "meta", attrib={
                                 "content": "XML2ePub 1.0b", "name": "generator"}, nsmap=NSMAP2)

    # Manifest
    file_name = etree.parse(file)
    TEI = file_name.getroot()
    NSMAP = {"tei": "http://www.tei-c.org/ns/1.0"}

    chapter_list = glob.glob(os.path.join(
        path, "tmp", "OEBPS", "Text", "*.xhtml"))
    for chapter in chapter_list:
        pathChap, name = os.path.split(chapter)
        etree.SubElement(manifest, "item", attrib={
                         "href": "Text/" + name, "id": name, "media-type": "application/xhtml+xml"})
        etree.SubElement(spine, "item", attrib={"idref": name})

    css_list = glob.glob(os.path.join(path, "tmp", "OEBPS", "Styles", "*.css"))
    for css in css_list:
        pathCSS, name = os.path.split(css)
        etree.SubElement(manifest, "item", attrib={
                         "href": "Styles/" + name, "id": name, "media-type": "text/css"})

    img_list = glob.glob(os.path.join(path, "tmp", "OEBPS", "Images", "*.*"))

    media_type = ""
    for img in img_list:
        pathImg, name = os.path.split(img)
        if (name.split(".")[1] == "jpg"):
            media_type = "jpeg"
        else:
            media_type = name.split(".")[1]

        etree.SubElement(manifest, "item", attrib={
                         "href": "Images/" + name, "id": name, "media-type": "image/" + media_type})

    etree.SubElement(manifest, "item", attrib={
                     "href": "toc.ncx", "id": "ncx", "media-type": "application/x-dtbncx+xml"})

    # Writing file
    path, filename = os.path.split(file)

    f = os.path.join(path, "tmp", "OEBPS", "content.opf")
    tree.write(f, xml_declaration=True, encoding='utf8',
               method="xml", pretty_print=True)


def generate_ncx(file, bk_metadata):
    """
    Generate the toc.ncx file
    """
    path, filename = os.path.split(file)

    ncxns = "http://www.daisy.org/z3986/2005/ncx/"
    opf = "{%s}" % ncxns
    NSMAP = {
        None: ncxns
    }

    # Tree
    # <!DOCTYPE ncx PUBLIC "-//NISO//DTD ncx 2005-1//EN" "http://www.daisy.org/z3986/2005/ncx-2005-1.dtd">
    ncx = etree.Element(opf + "ncx", attrib={"version": "2005-1"}, nsmap=NSMAP)
    tree = etree.ElementTree(ncx)
    head = etree.SubElement(ncx, "head")
    docTitle = etree.SubElement(ncx, "docTitle")
    navMap = etree.SubElement(ncx, "navMap")
    navPoint = etree.SubElement(navMap, "navPoint", attrib={
                                "id": "navPoint-1", "playOrder": "1"})
    navLabel = etree.SubElement(navPoint, "navLabel")

    etree.SubElement(navLabel, "text").text = "Start"

    etree.SubElement(navPoint, "content", attrib={
                     "src": "Text/Chapter1.xhtml"})

    etree.SubElement(head, "meta", attrib={"content": bk_metadata["uuid"]})
    etree.SubElement(head, "meta", attrib={
                     "content": "0", "name": "dtb:depth"})
    etree.SubElement(head, "meta", attrib={
                     "content": "0", "name": "dtb:totalPageCount"})
    etree.SubElement(head, "meta", attrib={
                     "content": "0", "name": "dtb:maxPageNumber"})

    etree.SubElement(docTitle, "text").text = bk_metadata["title"]

    path, filename = os.path.split(file)
    f = os.path.join(path, "tmp", "OEBPS", "toc.ncx")
    tree.write(f, xml_declaration=True, encoding='utf8', pretty_print=True)


def re_id(file):
    """
    Créate unique id for all div
    """
    try:
        print("[i] Ajout d'indentifiant au blocs de division...")
        path, filename = os.path.split(file)
        NSMAP = {"tei": "http://www.tei-c.org/ns/1.0"}
        tree = etree.parse(file)
        for id, div_chap in enumerate(tree.xpath('.//tei:div[re:match(@type, "act|appendix|article|book|bibliography|bibliographie|biography|biographie|chapter|colophon|conclusion|contents|dedication|epilogue|glossary|index|introduction|letter|notes|part|poem|preface|remerciement|scene|set|toc")]', namespaces={"tei": "http://www.tei-c.org/ns/1.0", "re": "http://exslt.org/regular-expressions"})):
            #print('[i] Traitement pour '+str(id))
            div_chap.set("kh_id", str("{0:03}".format(id + 1)))

        f = os.path.join(path, "001.xml")
        tree.write(f, xml_declaration=True, encoding='utf8', pretty_print=True)

    except Exception as e:
        logging.warn(e)


def add_container(file):
    """
    Add container.xml inside of the META-INF folder
    """
    path, filename = os.path.split(file)
    xml = r"""
        <container version="1.0" xmlns="urn:oasis:names:tc:opendocument:xmlns:container">
            <rootfiles>
                <rootfile full-path="OEBPS/content.opf" media-type="application/oebps-package+xml"/>
            </rootfiles>
        </container>
    """
    container = etree.fromstring(xml).getroottree()
    container.write(os.path.join(path, "tmp", "META-INF", "container.xml"),
                    encoding='utf8', pretty_print=True, xml_declaration=True)


def add_mimetype(file):
    """
    Add the mimetype file in the root of the ePub file
    """
    path, filename = os.path.split(file)
    mimetype = open(os.path.join(path, "tmp", "mimetype"), "w")
    mimetype.write('application/epub+zip')
    mimetype.close()


def generate_xhtml(file):
    path, filename = os.path.split(file)
    tmp_folder_path = os.path.join(path, "tmp", "OEBPS", "Text")
    tmp_folder_path2 = etree.XSLT.strparam(join(path, "tmp", "OEBPS", "Text"))
    parser = etree.XMLParser(ns_clean=True)
    file = LH.parse(file)
    TEI = file.getroot()

    # DRY : Namespace
    NSMAP = {"tei": "http://www.tei-c.org/ns/1.0"}

    had_preface = False
    had_toc = False
    had_conclusion = False

    print("[i] Conversion du fichier...")
    XSLT_attr = open(os.path.join("XSLT", "attr.xsl")).read()
    XSLT_split = open(os.path.join("XSLT", "split.xsl")).read()

    # Call saxon.jar for the conversion
    args = ['java', '-jar', os.path.join("D:\\", "git", "docx2XML", "Dependencies", "saxon9he.jar"), "-s:" + os.path.join(
        path, "001.xml"), "-xsl:" + os.path.join("D:\\", "git", "docx2XML", "XSLT", "attr.xsl"), "-o:" + os.path.join(path, "002.xml")]
    attr_result = saxonWrapper(*args)

    args = ['java', '-jar', os.path.join("D:\\", "git", "docx2XML", "Dependencies", "saxon9he.jar"), "-s:" + os.path.join(
        path, "002.xml"), "-xsl:" + os.path.join("D:\\", "git", "docx2XML", "XSLT", "split.xsl"), "-o:" + os.path.join(path, "003.xml")]
    splited_result = saxonWrapper(*args)

    print("[i] Création de la liste des xhtml...")
    xhtml_list = glob.glob("D:\\Khadafi\\*.xhtml")

    if (xhtml_list == []):
        print("/!\ Liste vide !")

    for xhtml in xhtml_list:
        shutil.copy(os.path.join("D:\\Khadafi", xhtml),
                    os.path.join(path, "tmp", "OEBPS", "Text"))


def get_ID(file):
    """
    Re-link all internal link
    """
    print("[i] Reliaison des liens...")
    path, filename = os.path.split(file)
    tmp_folder = os.path.join(path, "tmp", "OEBPS", "Text")
    xhtml_list = glob.glob(os.path.join(tmp_folder, "*.xhtml"))

    id_list = {}
    import lxml.html as htmlparse

    for xhtml in xhtml_list:
        tree = htmlparse.parse(xhtml)
        xhtml_path, xhtml_name = os.path.split(xhtml)

        psb_list = tree.xpath('//span[contains(@id, "psb_")]')
        for psb in psb_list:
            id_list["op" + psb.attrib["id"].split("_")[1]] = '"../Text/' + \
                xhtml_name + '#psb_' + \
                str(psb.attrib["id"].split("_")[1]) + '"'

        note_list = tree.xpath('//a[contains(@id, "note-")]')
        for id in note_list:
            id_list[id.attrib["id"]] = '"../Text/' + \
                xhtml_name + '#note-' + id.text + '"'

        appnote_list = tree.xpath('//a[contains(@id, "appnote-")]')
        for id in appnote_list:
            id_list[id.attrib["id"]] = '"../Text/' + \
                xhtml_name + '#appnote-' + id.text + '"'

    return id_list


def re_link(file, id_list):
    path, filename = os.path.split(file)
    tmp_folder = os.path.join(path, "tmp", "OEBPS", "Text")
    xhtml_list = glob.glob(os.path.join(tmp_folder, "*.xhtml"))

    for xhtml in xhtml_list:
        xhtml_path, xhtml_name = os.path.split(xhtml)
        text = ""
        with open(xhtml, 'r', encoding="utf8") as f:
            text = f.read()
            for id in id_list:
                rgx = re.compile('("#' + id + '")')
                text = re.sub(rgx, id_list[id], text)
        with open(xhtml, "w", encoding="utf8") as f:
            f.write(text)


# TODO : Set the note to foot
def re_note(file):
    path, filename = os.path.split(file)

    class Note:

        def __init__(self):
            self.note = ""


def main():
    # TODO :
    # Adding XML checker to test if the XML is well formed and support the conversion
    file_name = get_file()
    re_id(file_name)
    book_metadata = get_metadata(file_name)
    tmp_fldr = tmp_folder(file_name)
    copy_graphics(file_name)
    copy_css(file_name)
    add_container(file_name)
    add_mimetype(file_name)
    generate_xhtml(file_name)
    generate_container(file_name, book_metadata)
    generate_ncx(file_name, book_metadata)

    # TODO : A méditer pour la fonctionnement
    id_list = get_ID(file_name)
    re_link(file_name, id_list)

    # Antsoina farany satria io no mamafa ny dossier temporaire
    # sy mi-renomer ilay fichier zip ho epub
    zip_folder(file_name)

if __name__ == "__main__":
    main()
