# docx2XML & XML2ePub v0.1b
## What is this ?

This is a script that convert ```.docx``` > ```.xml``` with ```docx2XML.py``` or ```.xml``` > ```.epub``` with ```XML2ePub.py```.

**NOTE : ** It's for specific type of XML for now. Only TEI is supported.

## Dependencies

* python 3.4
* python-docx
* lxml
* JRE or JDK (version 7 or more)
* Saxon-EE processor
* pywin32 (**NOTE :** Must add to system path the path of the ```C:\Python34\Lib\site-packages\pywin32_system32```)

**NOTE** : You can find the binaries installer of ```lxml``` and ```python-docx``` inside of ```./Dependencies/``` folder.

## Installation

The script doesn't need any installation but you must install the dependencies to run the script.
```python
python -m pip install lxml
python -m pip install python-docx
```

## Run

You must specify the file name by ```-f``` argument on your terminal.

```python
# If you want convert MS Word (.docx) to XML
python docx2XML.py -f file_name.docx

# If you want convert XML to ePub
python XML2ePub.py -f file_name.xml
```
**NOTE** : For now, it doesn't support many file at the same time.

## Author

&copy; [Hantsaniala Eléo](https://www.facebook.com/hantsaniala) 2016
