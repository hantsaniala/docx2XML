<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns="http://www.w3.org/1999/xhtml"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ns0="http://www.w3.org/1999/xhtml"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:re="http://exslt.org/regular-expressions" exclude-result-prefixes="ns0 xs tei re"
    version="2.0">
    <xsl:output method="xml" encoding="utf-8" omit-xml-declaration="yes" indent="no"/>

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

    <!-- page-start-break -->
    <!-- pb tokony ho ao anaty p -->
    <xsl:template match="tei:pb">
        <xsl:if test='matches(name(parent::node()[1]), "argument|list|p|quote|listBibl|lg|table|item|l")'>
            <xsl:if test='not(@ed="temoin")'>
                <xsl:element name="span">
                    <xsl:attribute name="id">psb_<xsl:value-of select="@n"/>
                    </xsl:attribute>
                    <xsl:attribute name="class">page-start-break</xsl:attribute>
                    <xsl:value-of select="@n"/>
                </xsl:element>
            </xsl:if>
            <xsl:if test='tei:pb[@ed="temoin"]'>
                <xsl:element name="span">
                    <xsl:attribute name="class">temoin</xsl:attribute>
                    <xsl:value-of select="not(string())"/>
                </xsl:element>
            </xsl:if>
        </xsl:if>

        <xsl:if test='matches(name(parent::node()[1]), "div")'>
            <xsl:if test='not(@ed="temoin")'>
                <xsl:element name="p">
                    <xsl:attribute name="class">text-standard</xsl:attribute>
                    <xsl:element name="span">
                        <xsl:attribute name="id">psb_<xsl:value-of select="@n"/>
                        </xsl:attribute>
                        <xsl:attribute name="class">page-start-break</xsl:attribute>
                        <xsl:value-of select="@n"/>
                    </xsl:element>
                </xsl:element>
            </xsl:if>
            <xsl:if test='@ed="temoin"'>
                <xsl:element name="p"><xsl:attribute name="class">temoin</xsl:attribute><xsl:value-of select="@n"/></xsl:element>
            </xsl:if>
        </xsl:if>
    </xsl:template>
	
    <!-- p rendition -->
    <!-- Mila jerena hoe rend sa rendition no tena izy -->
    <xsl:template match="tei:div/tei:p[@rendition|@rend]">
        <xsl:element name="p">
            <xsl:if test='(@rendition = "#center") or (@rend = "center")'>
                <xsl:attribute name="class">text-center</xsl:attribute>
            </xsl:if>
            <xsl:if test='(@rendition = "#right") or (@rend = "right")'>
                <xsl:attribute name="class">text-right</xsl:attribute>
            </xsl:if>
            <xsl:if test='@rendition = "#noepub"'>
                <xsl:attribute name="class">no-epub</xsl:attribute>
            </xsl:if>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>

    <!-- p standard -->
    <xsl:template match="tei:div/tei:p[not(@*)]">
        <xsl:element name="p">
            <xsl:attribute name="class">text-standard</xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>
	
	<xsl:template match="tei:label">
		<xsl:element name="p">
			<xsl:attribute name="class">
				<xsl:value-of select="name(.)"/>
				<xsl:if test='@place'>
					<xsl:text> </xsl:text>
					<xsl:value-of select="name(.)"/>-<xsl:value-of select="@place"/>
				</xsl:if>
				<xsl:if test='@rend|@rendition'>
					<xsl:text> </xsl:text>
					<xsl:value-of select="name(.)"/>-<xsl:value-of select="@rend|@rendition"/>
				</xsl:if>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>


    <!-- quote -->
    <xsl:template match="tei:quote/tei:p">
            <xsl:element name="p">
                <xsl:attribute name="class">
							<xsl:text>quote-text</xsl:text>
							<xsl:if test='@rend'>
								<xsl:text> quote-</xsl:text>
								<xsl:value-of select="@rend"/>
							</xsl:if>
						</xsl:attribute>
                <xsl:apply-templates select="node()"/>
            </xsl:element>
    </xsl:template>
	
    <xsl:template match="tei:said">
        <xsl:element name="span">
            <xsl:attribute name="class">text-said</xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>

    <!-- figure -->
    <xsl:template match="tei:div/tei:figure|tei:docImprint/tei:figure|tei:argument/tei:figure|tei:item/tei:figure|tei:cell/tei:figure">
        <xsl:element name="div">
            <xsl:attribute name="class">bloc-image</xsl:attribute>
            <xsl:element name="img">
                <xsl:attribute name="class">image</xsl:attribute>
                <xsl:attribute name="src">../Images/<xsl:value-of select="substring-after(node()/@url, '/')"/></xsl:attribute>
                <xsl:attribute name="alt"><xsl:value-of select="substring-after(node()/@url, '/')"/></xsl:attribute>
            </xsl:element>
        </xsl:element>
    </xsl:template>
	
    <xsl:template match="tei:cell/tei:graphic">
        <xsl:element name="div">
            <xsl:attribute name="class">bloc-image</xsl:attribute>
            <xsl:element name="img">
                <xsl:attribute name="class">image</xsl:attribute>
                <xsl:attribute name="src">../Images/<xsl:value-of select="substring-after(@url, '/')"/></xsl:attribute>
                <xsl:attribute name="alt"><xsl:value-of select="substring-after(@url, '/')"/></xsl:attribute>
            </xsl:element>
        </xsl:element>
    </xsl:template>

    <!-- caption -->
    <xsl:template match="tei:div/tei:caption">
        <xsl:element name="p">
            <xsl:attribute name="class">text-legende</xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>

    <!-- lg -->
    <xsl:template match="tei:lg">
        <xsl:element name="ul">
            <xsl:attribute name="class">quote-text</xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>
	
    <xsl:template match="tei:l">
        <xsl:element name="li">
            <xsl:attribute name="class">quote-text</xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>

    <!-- ref -->
    <xsl:template match="tei:ref">
        <xsl:element name="a">
            <xsl:attribute name="href"><xsl:value-of select="@target"/></xsl:attribute>
            <xsl:if test='matches(@target, "http*")'>
                <xsl:attribute name="class">link link-extern</xsl:attribute>
            </xsl:if>
            <xsl:if test='matches(@target, "#note*")'>
                <xsl:attribute name="class">link note-corps</xsl:attribute>
                <xsl:attribute name="id">
                    <xsl:value-of select="@xml:id"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:if test='matches(@target, "#appnote*")'>
                <xsl:attribute name="class">link note-corps</xsl:attribute>
                <xsl:attribute name="id">
                    <xsl:value-of select="@xml:id"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:if test='matches(@target, "#op*")'>
                <xsl:attribute name="class">link link-intern</xsl:attribute>
            </xsl:if>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>

    <!-- liste -->
    <xsl:template match="tei:list|tei:listBibl">
        <xsl:choose>
            <xsl:when test='@type="toc"'>
                <xsl:element name="ul">
                    <xsl:attribute name="class">toc</xsl:attribute>
                    <xsl:apply-templates select="node()"/>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="ul">
                    <xsl:apply-templates select="node()"/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
	
    <xsl:template match="tei:list/tei:item|tei:listBibl/tei:bibl">
        <xsl:element name="li">
            <xsl:if test='../@type="toc"'>
                <xsl:attribute name="class">toc</xsl:attribute>
            </xsl:if>
        <xsl:if test="@n"><xsl:value-of select="@n"/>&#160;</xsl:if><xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>
	
    <xsl:template match="tei:list/tei:item/tei:p">
        <xsl:element name="p">
            <xsl:attribute name="class">text-list</xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>

    <!-- argument -->
    <xsl:template match="tei:argument/tei:p">
        <xsl:element name="p">
            <xsl:attribute name="class">argument-text</xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>

    <!-- head -->
    <xsl:template match='tei:div[matches(@type, "act|appendix|article|book|bibliography|bibliographie|biography|biographie|colophon|conclusion|contents|chapter|dedication|epilogue|glossary|index|introduction|letter|notes|poem|part|preface|remerciement|section|ssection|sssection|scene|set|toc")]'>
        <xsl:element name="div">
            <xsl:attribute name="class"><xsl:value-of select="@type"/></xsl:attribute>
				<xsl:if test="@kh_id">
					<xsl:attribute name="kh_id"><xsl:value-of select="@kh_id"/></xsl:attribute>
				</xsl:if>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="tei:head">
        <xsl:choose>
            <xsl:when test='../@type="part"'>
                <xsl:if test='../@type="part"'>
                    <xsl:element name="h1">
                        <xsl:attribute name="class">part</xsl:attribute>
                        <xsl:apply-templates select="node()"/>
                    </xsl:element>
                </xsl:if>
                <xsl:if
                    test='matches(../@type, "act|appendix|article|book|bibliography|bibliographie|biography|biographie|chapter|colophon|conclusion|contents|dedication|epilogue|glossary|index|introduction|letter|listBibl|notes|poem|preface|remerciement|scene|set|toc")'>
                    <xsl:element name="h2">
                        <xsl:attribute name="class">titre-1</xsl:attribute>
                        <xsl:apply-templates select="node()"/>
                    </xsl:element>
                </xsl:if>
                <xsl:if test='../@type="section"'>
                    <xsl:element name="h3">
                        <xsl:attribute name="class">section</xsl:attribute>
                        <xsl:apply-templates select="node()"/>
                    </xsl:element>
                </xsl:if>
                <xsl:if test='../@type="ssection"'>
                    <xsl:element name="h4">
                        <xsl:attribute name="class">ssection</xsl:attribute>
                        <xsl:apply-templates select="node()"/>
                    </xsl:element>
                </xsl:if>
                <xsl:if test='../@type="sssection"'>
                    <xsl:element name="h5">
                        <xsl:attribute name="class">sssection</xsl:attribute>
                        <xsl:apply-templates select="node()"/>
                    </xsl:element>
                </xsl:if>
            </xsl:when>
            
            <xsl:otherwise>
                <xsl:if
                    test='matches(../@type, "act|appendix|article|book|bibliography|bibliographie|biography|biographie|chapter|colophon|conclusion|contents|dedication|epilogue|glossary|index|introduction|letter|listBibl|notes|poem|preface|remerciement|scene|set|toc")'>
                    <xsl:element name="h1">
                        <xsl:attribute name="class">titre-1</xsl:attribute>
                        <xsl:apply-templates select="node()"/>
                    </xsl:element>
                </xsl:if>
                <xsl:if test='../@type="section"'>
                    <xsl:element name="h2">
                        <xsl:attribute name="class">section</xsl:attribute>
                        <xsl:apply-templates select="node()"/>
                    </xsl:element>
                </xsl:if>
                <xsl:if test='../@type="ssection"'>
                    <xsl:element name="h3">
                        <xsl:attribute name="class">ssection</xsl:attribute>
                        <xsl:apply-templates select="node()"/>
                    </xsl:element>
                </xsl:if>
                <xsl:if test='../@type="sssection"'>
                    <xsl:element name="h4">
                        <xsl:attribute name="class">sssection</xsl:attribute>
                        <xsl:apply-templates select="node()"/>
                    </xsl:element>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:list/tei:head|tei:lg/tei:head">
        <xsl:element name="p">
            <xsl:attribute name="class">list-head</xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>
	
    <xsl:template match="tei:table/tei:head">
        <xsl:element name="p">
            <xsl:attribute name="class">table-head</xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>

    <!-- TODO : Table-->
    <xsl:template match="tei:table">
        <xsl:element name="table">
            <xsl:choose>
                <xsl:when test='@rend="border"'>
                    <xsl:attribute name="class">table table-border</xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="class">table</xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="tei:row">
        <xsl:element name="tr">
            <xsl:attribute name="class">
                <xsl:value-of select="name(.)"/>
                <xsl:if test="@rend">
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="name(.)"/>-<xsl:value-of select="@rend"/>
                </xsl:if>
            </xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="tei:cell">
        <xsl:element name="td">
            <xsl:attribute name="class">
                <xsl:value-of select="name(.)"/>
                <xsl:if test="@rend">
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="name(.)"/>-<xsl:value-of select="@rend"/>
                </xsl:if>
            </xsl:attribute>
            <xsl:element name="p">
                <xsl:attribute name="class">table-text</xsl:attribute>
                <xsl:apply-templates select="node()"/>
            </xsl:element>
        </xsl:element>
    </xsl:template>
	<xsl:template match="tei:cell/tei:p">
        <xsl:element name="p">
            <xsl:attribute name="class">table-text</xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>
	

    <xsl:template match="tei:note">
        <xsl:element name="a">
            <xsl:attribute name="class">note-corps</xsl:attribute>
            <xsl:attribute name="id">bdftn<xsl:value-of select="@n"/></xsl:attribute>
            <xsl:attribute name="href">#ftn<xsl:value-of select="@n"/></xsl:attribute>
            <xsl:value-of select="@n"/>
        </xsl:element>
        <xsl:element name="span">
            <xsl:attribute name="class">text-note</xsl:attribute>
            <xsl:element name="a">
                <xsl:attribute name="id">ftn<xsl:value-of select="@n"/></xsl:attribute>
                <xsl:attribute name="href">#bdftn<xsl:value-of select="@n"/></xsl:attribute>
                <xsl:attribute name="class">note-foot</xsl:attribute>
                <xsl:value-of select="@n"/>
            </xsl:element>&#160;<xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="tei:note/tei:p">
        <xsl:apply-templates select="node()"/>
    </xsl:template>

    <xsl:template match="tei:postscript/tei:p">
        <xsl:element name="p">
            <xsl:attribute name="class">postscript-text</xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="tei:postscript|tei:opener|tei:closer|tei:bibl|tei:argument|tei:quote">
        <xsl:element name="div">
            <xsl:attribute name="class">
				<xsl:value-of select="name(.)"/>
				<xsl:if test='@place'>
					<xsl:text> </xsl:text>
					<xsl:value-of select="name(.)"/>-<xsl:value-of select="@place"/>
				</xsl:if>
				<xsl:if test='@rend|@rendition'>
					<xsl:text> </xsl:text>
					<xsl:value-of select="name(.)"/>-<xsl:value-of select="@rend|@rendition"/>
				</xsl:if>
			</xsl:attribute><xsl:attribute name="class"><xsl:value-of select="name(.)"/></xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="tei:closer/tei:signed|tei:closer/tei:salute|tei:placeName|tei:date|tei:add|tei:name">
        <xsl:element name="span">
            <xsl:attribute name="class"><xsl:value-of select="name(.)"/></xsl:attribute>
            <xsl:if test="@when">
                <xsl:element name="span">
                    <xsl:attribute name="class">date-when</xsl:attribute>
                    <xsl:value-of select="@when"/>
                </xsl:element>
            </xsl:if>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="tei:byline">
        <xsl:element name="p">
            <xsl:attribute name="class"><xsl:value-of select="name(.)"/></xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>
	
	<xsl:template match="tei:dateline">
        <xsl:element name="p">
            <xsl:attribute name="class"><xsl:value-of select="name(.)"/></xsl:attribute>
            <xsl:if test="@rend">
					<xsl:attribute name="class">text-<xsl:value-of select="@rend"/></xsl:attribute>
            </xsl:if>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match='tei:div[matches(@type, "letter|section|ssectioin|sssection")]/tei:signed'>
        <xsl:element name="p">
            <xsl:attribute name="class"><xsl:value-of select="name(.)"/></xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="tei:epigraph">
        <xsl:apply-templates select="node()"/>
    </xsl:template>
    <xsl:template match="tei:epigraph/tei:p">
        <xsl:element name="p">
            <xsl:attribute name="class">text-exergue</xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="tei:epigraph/tei:bibl">
        <xsl:element name="p">
            <xsl:attribute name="class">text-auteur-exergue</xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>
    <xsl:template match='tei:div[@type="appendix"]/tei:listBibl/tei:bibl'>
        <xsl:element name="p">
            <xsl:attribute name="class">text-bible</xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>
    <xsl:template match='tei:div[@type="appendix"]/tei:listBibl/tei:head'>
        <xsl:element name="p">
            <xsl:attribute name="class">appendix-head</xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>
    
    <!-- fw -->
    <xsl:template match="tei:fw[@rend='tc']">
        <xsl:element name="span">
            <xsl:attribute name="class">curent-title</xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>

    <!-- lb -->
    <xsl:template match="tei:lb">
    <!--<xsl:template match="tei:p/tei:lb|tei:hi/tei:lb|tei:head/tei:lb|tei:list/tei:lb|tei:cell/tei:lb|tei:ab/tei:lb|tei:salute/tei:lb|tei:closer/tei:lb|tei:signed/tei:lb|tei:dateline/tei:lb">-->
        <xsl:element name="br" namespace="http://www.w3.org/1999/xhtml"/>
    </xsl:template>
    <xsl:template match="tei:div/tei:lb">
        <xsl:element name="p"><xsl:attribute name="class"
            >text-standard</xsl:attribute>&#160;</xsl:element>
    </xsl:template>

    <!-- ab -->
    <xsl:template match="tei:ab">
        <xsl:element name="p">
            <xsl:attribute name="class">text-sep</xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>
    <!-- Ho hita eo ihany raha havadika auto ireo séparateur na ho atao manuel-->

    <!-- emph -->
    <xsl:template match="tei:emph">
        <xsl:element name="span">
            <xsl:attribute name="class">text-italic</xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>

    <!-- hi rend="" -->
    <xsl:template match="tei:hi">
        <xsl:element name="span">
            <xsl:if test='@rend = "b"'>
                <xsl:attribute name="class">text-bold</xsl:attribute>
            </xsl:if>
            <xsl:if test='@rend = "u"'>
                <xsl:attribute name="class">text-underline</xsl:attribute>
            </xsl:if>
            <xsl:if test='@rend = "sup"'>
                <xsl:attribute name="class">text-sup</xsl:attribute>
            </xsl:if>
            <xsl:if test='@rend = "sub"'>
                <xsl:attribute name="class">text-sub</xsl:attribute>
            </xsl:if>
            <xsl:if test='@rend = "sc"'>
                <xsl:attribute name="class">text-pcap</xsl:attribute>
            </xsl:if>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>

</xsl:stylesheet>