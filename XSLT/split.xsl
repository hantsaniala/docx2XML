<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:tei="http://www.tei-c.org/ns/1.0" 
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="xs tei xsl" version="2.0">
    <xsl:output method="html" encoding="utf-8" omit-xml-declaration="yes" indent="no"/>
    <xsl:param name="tmpPath"/>
    <!-- Split the file by div -->
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
    <!-- Copyright -->
    <xsl:template match='tei:titlePage/tei:argument'>
            <xsl:result-document href="/D:/Khadafi/Copyright.xhtml" method="html" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
                <xsl:element name="html" namespace="http://www.w3.org/1999/xhtml">
                    <xsl:element name="head" namespace="http://www.w3.org/1999/xhtml">
                        <xsl:element name="title" namespace="http://www.w3.org/1999/xhtml">coyright</xsl:element>
                        <xsl:element name="link" namespace="http://www.w3.org/1999/xhtml">
                            <xsl:attribute name="href">../Styles/styles.css</xsl:attribute>
                            <xsl:attribute name="rel">stylesheet</xsl:attribute>
                            <xsl:attribute name="type">text/css</xsl:attribute>
                        </xsl:element>
                    </xsl:element>
                    <xsl:element name="body" namespace="http://www.w3.org/1999/xhtml">
                        <xsl:apply-templates select="node()"/>
                        <xsl:element name="p"><xsl:attribute name="class">end</xsl:attribute></xsl:element>
                    </xsl:element>
                </xsl:element>
            </xsl:result-document>
    </xsl:template>
    
    <!-- Body -->
	<xsl:template match='xhtml:div[@kh_id]'>
            <xsl:result-document href="/D:/Khadafi/Chapter{@kh_id}.xhtml" method="html" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" encoding="UTF-8">
                <xsl:element name="html" namespace="http://www.w3.org/1999/xhtml">
                    <xsl:element name="head" namespace="http://www.w3.org/1999/xhtml">
                        <xsl:element name="title" namespace="http://www.w3.org/1999/xhtml"><xsl:value-of select='@type'/></xsl:element>
                        <xsl:element name="link" namespace="http://www.w3.org/1999/xhtml">
                            <xsl:attribute name="href">../Styles/styles.css</xsl:attribute>
                            <xsl:attribute name="rel">stylesheet</xsl:attribute>
                            <xsl:attribute name="type">text/css</xsl:attribute>
                        </xsl:element>
                    </xsl:element>
                    <xsl:element name="body" namespace="http://www.w3.org/1999/xhtml">
                        <xsl:apply-templates select="node()"/>
                        <xsl:element name="p"><xsl:attribute name="class">end</xsl:attribute></xsl:element>
                    </xsl:element>
                </xsl:element>
            </xsl:result-document>
    </xsl:template>
</xsl:stylesheet>